import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Favourite from '../views/Favourite.vue';
import Offline from '../views/Offline.vue';
import NewsDetail from '../views/NewsDetail.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/favourite',
    name: 'Favourite',
    component: Favourite,
  },
  {
    path: '/Offline',
    name: 'Offline',
    component: Offline,
  },
  {
    path: '/news-detail/:id',
    name: 'NewsDetail',
    component: NewsDetail,
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue'),
  },
];

const router = new VueRouter({
  routes,
});

export default router;
